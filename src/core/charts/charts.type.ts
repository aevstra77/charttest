export type { ChartData, ChartDataset } from "chart.js";

export type TChartLine = { label: string; color: string };
