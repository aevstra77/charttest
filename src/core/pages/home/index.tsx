import React, { FC, memo, useCallback, useEffect, useState } from "react";
import { Line } from "react-chartjs-2";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";

import { TChartLine } from "../../charts/charts.type";
import { unknownDataService } from "../../../services/unknownData/unknownData.service";
import { unknownDataFactory } from "../../../domain/unknownData/unknownData.factory";
import { IUnknownDataModel } from "../../../domain/unknownData/unknownData.model";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

const verticalDataChartLines: Record<string, TChartLine> = {
  y: { label: "y", color: "red" },
  wk: { label: "wk", color: "green" },
  session_view: { label: "session_view", color: "yellow" },
  hits_view: { label: "hits_view", color: "black" },
  adv_view_all: { label: "adv_view_all", color: "orange" },
  hits_tocart: { label: "hits_tocart", color: "blue" },
  revenue: { label: "revenue", color: "lightblue" },
  ordered_units: { label: "ordered_units", color: "lighntred" },
  cancellations: { label: "cancellations", color: "lightgreen" },
  returns: { label: "returns", color: "lightyellow" },
  cpm: { label: "cpm", color: "gray" },
  cpo: { label: "cpo", color: "lightgray" },
};

export const HomePage: FC = memo(() => {
  const [chartData, setChartData] = useState<IUnknownDataModel>();

  const getMainGraphData = useCallback(async () => {
    const data = await unknownDataService.getMainPGGraphData({
      api_id: "133183",
      brand_id: "31102019",
      category_id: "17027491",
    });
    setChartData(data);
  }, []);

  useEffect(() => {
    try {
      void getMainGraphData();
    } catch (e) {
      alert("Something went wrong");
      console.log(e);
    }
  }, [getMainGraphData]);

  return (
    <div style={{ height: "100vh" }}>
      {chartData && (
        <>
          <Line
            style={{ maxHeight: "50%" }}
            data={unknownDataFactory.modelToHorizontalDataLineChartData(
              chartData,
              verticalDataChartLines
            )}
          ></Line>
          <Line
            style={{ maxHeight: "50%" }}
            data={unknownDataFactory.modelToVerticalDataLineChartData(
              chartData
            )}
          ></Line>
        </>
      )}
    </div>
  );
});
