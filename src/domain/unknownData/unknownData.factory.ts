import { IUnknownDataModel } from "./unknownData.model";
import { unknownDataUtil } from "./unknownData.util";

import {
  TChartLine,
  ChartData,
  ChartDataset,
} from "../../core/charts/charts.type";
import { commonUtil } from "../../utils/common.util";

class UnknownDataFactory {
  modelToHorizontalDataLineChartData = (
    model: IUnknownDataModel,
    lines: Record<string, TChartLine>
  ): ChartData<"line"> => ({
    datasets: Object.entries(model).map(([key, value]) =>
      this.modelToHorizontalDataLineChartDataset(lines[key], value)
    ),
    labels: new Array(unknownDataUtil.getUnknownDataMaxLength(model))
      .fill(0)
      .map((item, index) => `${index + 1}`),
  });

  modelToHorizontalDataLineChartDataset = (
    line: TChartLine,
    value: number[]
  ): ChartDataset<"line"> => ({
    label: line.label,
    borderColor: line.color,
    data: value,
  });

  modelToVerticalDataLineChartData = (
    model: IUnknownDataModel
  ): ChartData<"line"> => ({
    datasets: new Array(unknownDataUtil.getUnknownDataMaxLength(model))
      .fill([])
      .map((item, index) =>
        this.modelToVerticalDataLineChartDataset(model, index)
      ),
    labels: Object.keys(model),
  });

  modelToVerticalDataLineChartDataset = (
    model: IUnknownDataModel,
    index: number
  ): ChartDataset<"line"> => ({
    data: Object.values(model).reduce(
      (acc, value) => [...acc, value[index]],
      [] as number[]
    ),
    label: `${index + 1}`,
    borderColor: commonUtil.getRandomColor(),
  });
}

export const unknownDataFactory = new UnknownDataFactory();
