export interface IUnknownDataModel {
  y: number[];
  wk: number[];
  session_view: number[];
  hits_view: number[];
  adv_view_all: number[];
  hits_tocart: number[];
  revenue: number[];
  ordered_units: number[];
  cancellations: number[];
  returns: number[];
  cpm: number[];
  cpo: number[];
}
