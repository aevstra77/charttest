import { IUnknownDataModel } from "./unknownData.model";

class UnknownDataUtil {
  getUnknownDataMaxLength = (model: IUnknownDataModel): number =>
    Object.values(model).reduce(
      (acc, data) => (data.length > 0 ? data.length : acc),
      0
    );
}

export const unknownDataUtil = new UnknownDataUtil();
