import { IUnknownDataModel } from "../../domain/unknownData/unknownData.model";

class UnknownDataService {
  getMainPGGraphData = async (params: {
    api_id: string;
    brand_id: string;
    category_id: string;
  }): Promise<IUnknownDataModel> => {
    const response = await fetch(
      `http://62.84.124.35:5051/api/v1/week/functions/main_pg_fullgraph?${new URLSearchParams(
        params
      )}`,
      {
        method: "GET",
        mode: "cors",
        headers: {
          Accept: "application/json",
        },
      }
    );
    return await response.json();
  };
}

export const unknownDataService = new UnknownDataService();
